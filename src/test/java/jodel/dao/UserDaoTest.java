package jodel.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.PrepareTests;
import jodel.model.Tuser;


public class UserDaoTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		PrepareTests.initDatabase();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetInstance() {
		UserDao u = UserDao.getInstance();
		assertNotNull(u);
	}

	@Test
	public void testGetTuser() {
		UserDao u = UserDao.getInstance();
		Tuser user = new Tuser();
		
		user.setFirstname("Hans");
		user.setLastname("Maier");
		user.setActive((byte) 0);
		user.setAdmin((byte)1);
		user.setEmail("hans.maier@admin.de");
		user.setLoginname("hansi");
		user.setPassword("geheim");
		
		u.saveTuser(user);
		Tuser test = u.getTuser(user.getOid());
		assertNotNull(test);
	}
	
	@Test
	public void testGetTuserByLoginName() {
		UserDao u = UserDao.getInstance();
		Tuser user = new Tuser();
		
		user.setFirstname("TestGetUser");
		user.setLastname("ByLoginName");
		user.setActive((byte) 0);
		user.setAdmin((byte)1);
		user.setEmail("l@admin.de");
		user.setLoginname("hansi325");
		user.setPassword("geheim");
		
		u.saveTuser(user);
		Tuser test = u.getTuserByLoginName(user.getLoginname());
		assertNotNull(test);
	}
	
	@Test
	public void testGetTusers() {
		UserDao u = UserDao.getInstance();
		Tuser user;
		for (long i=0; i < 10; ++i) {

			user = new Tuser();
			user.setFirstname("Hans" + i);
			user.setLastname("Maier" + i);
			user.setActive((byte) 0);
			user.setAdmin((byte)1);
			user.setEmail("hans.maier@admin.de" + i);
			user.setLoginname("hansi" + i);
			user.setPassword("geheim" + i);
			
			u.saveTuser(user) ;
		}
		List<Tuser> users = u.getTusers();
		assertTrue(users.size()>=10);
	}

	@Test
	public void testSaveTuser() {
		UserDao userDao = UserDao.getInstance();

		Tuser user = new Tuser();
		user.setFirstname("Testing");
		user.setLastname("Save");
		user.setActive((byte) 0);
		user.setAdmin((byte)1);
		user.setEmail("Testing.Save@admin.de");
		user.setLoginname("hansi");
		user.setPassword("geheim");
		
		userDao.saveTuser(user) ;
		Tuser users = userDao.getTuser(user.getOid());
		assertNotNull(users);
	}

	@Test
	public void testDeleteTusers() {
		UserDao userDao = UserDao.getInstance();
		List<Tuser> users = userDao.getTusers();
		for (Tuser user: users) {
			userDao.deleteTuser(user.getOid());
		}
		users = userDao.getTusers();
		assertTrue(users.size()<1);
	}

}
