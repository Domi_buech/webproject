package jodel.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.PrepareTests;
import jodel.model.Tcompany;

public class CompanyDaoTest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		PrepareTests.initDatabase();
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetInstance() {
		CompanyDao c = CompanyDao.getInstance();
		assertNotNull(c);
	}

	@Test
	public void testGetCompany() {
		CompanyDao c = CompanyDao.getInstance();
		Tcompany com = new Tcompany();
		
		com.setName("Company Testing Get");
		com.setDepartment("IT");
		com.setStreetnr("24/B");
		com.setStreet("Valley Drive");
		com.setPostalcode(12345);
		com.setCountry("USA");
		c.saveTcompany(com);
		
		Tcompany test = c.getTcompany(com.getOid());
		assertNotNull(test);
	}

	@Test
	public void testGetCompanies() {
		CompanyDao c = CompanyDao.getInstance();
		Tcompany com;
		
		for (long i = 0; i < 10; ++i) {
			com = new Tcompany();
			com.setName("The Company with the number " + i);
			com.setDepartment("Marketing");
			com.setStreetnr("101/C");
			com.setStreet("Greek Drive");
			com.setPostalcode(54321);
			com.setCountry("Wakanda");
			c.saveTcompany(com);
		}
		List<Tcompany> companies = c.getTcompanies();
		System.out.println("Company Test " + companies);
		assertTrue(companies.size() > 0);
	}

	@Test
	public void testSaveCompany() {
		CompanyDao c = CompanyDao.getInstance();
		Tcompany com = new Tcompany();
		
		com.setName("Company Testing Save");
		com.setDepartment("Marketing");
		com.setStreetnr("101/C");
		com.setStreet("Greek Drive");
		com.setPostalcode(54321);
		com.setCountry("Wakanda");
		
		c.saveTcompany(com);
		Tcompany coms = c.getTcompany(com.getOid());
		assertNotNull(coms);
	}

	@Test
	public void testDeleteCompany() {
		CompanyDao c = CompanyDao.getInstance();
		List<Tcompany> companies = c.getTcompanies();
		for (Tcompany com : companies) {
			c.deleteTcompany(com.getOid());
		}
		companies = c.getTcompanies();
		assertTrue(companies.size() < 1);
	}

}
