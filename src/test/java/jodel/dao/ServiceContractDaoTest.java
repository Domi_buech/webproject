package jodel.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.PrepareTests;

import jodel.model.Tservicecontract;


public class ServiceContractDaoTest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		PrepareTests.initDatabase();
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetInstance() {
		ServiceContractDao c = ServiceContractDao.getInstance();
		assertNotNull(c);
	}

	@Test
	public void testGetServiceContract() {
		ServiceContractDao c = ServiceContractDao.getInstance();
		Tservicecontract sercon = new Tservicecontract();
		
		Calendar cal = Calendar.getInstance();
		Date d1 = cal.getTime();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d2 = cal.getTime();
		
		sercon.setStart(d1);
		sercon.setEnd(d2);
		
		c.saveTservicecontract(sercon);
		Tservicecontract test = c.getTservicecontract(sercon.getOid());
		assertNotNull(test);
	}

	@Test
	public void testGetServiceContracts() {
		ServiceContractDao c = ServiceContractDao.getInstance();
		Tservicecontract sercon;
		
		for (long i = 0; i < 10; ++i) {			
			sercon = new Tservicecontract();
			
			Calendar cal = Calendar.getInstance();
			Date d1 = cal.getTime();
			cal.add(Calendar.YEAR, (int) i); // increases date by i years
			Date d2 = cal.getTime();
			
			sercon.setStart(d1);
			sercon.setEnd(d2);
			c.saveTservicecontract(sercon);
		}
		List<Tservicecontract> serviceContracts = c.getTservicecontracts();
		assertTrue(serviceContracts.size() >= 10);
	}

	@Test
	public void testSaveServiceContract() {
		ServiceContractDao c = ServiceContractDao.getInstance();
		Tservicecontract sercon = new Tservicecontract();
		
		Calendar cal = Calendar.getInstance();
		Date today = cal.getTime();
		cal.add(Calendar.YEAR, 10); // add 10 years to current year
		Date increasedDate = cal.getTime();

		sercon.setStart(today);
		sercon.setEnd(increasedDate);
		c.saveTservicecontract(sercon);
		Tservicecontract test = c.getTservicecontract(sercon.getOid());
		assertNotNull(test);
	}

	@Test
	public void testDeleteServiceContract() {
		ServiceContractDao c = ServiceContractDao.getInstance();
		List<Tservicecontract> serviceContracts = c.getTservicecontracts();
		for (Tservicecontract sercon : serviceContracts) {
			c.deleteTservicecontract(sercon.getOid());
		}
		serviceContracts = c.getTservicecontracts();
		assertTrue(serviceContracts.size() < 1);
	}

}
