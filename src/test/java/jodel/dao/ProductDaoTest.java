package jodel.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.PrepareTests;
import jodel.model.Tproduct;


public class ProductDaoTest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		PrepareTests.initDatabase();

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetInstance() {
		ProductDao c = ProductDao.getInstance();
		assertNotNull(c);
	}

	@Test
	public void testGetProduct() {
		ProductDao c = ProductDao.getInstance();
		Tproduct pro = new Tproduct();
		pro.setVersion("Product Testing Get");
		c.saveTproduct(pro);
		
		Tproduct test = c.getTproduct(pro.getOid());
		assertNotNull(test);
	}

	@Test
	public void testGetProducts() {
		ProductDao c = ProductDao.getInstance();
		Tproduct pro;
		
		for (long i = 0; i < 10; ++i) {
			pro = new Tproduct();
			pro.setVersion("The Product with the number " + i);
			c.saveTproduct(pro);
		}
		List<Tproduct> products = c.getTproducts();
		assertTrue(products.size() >= 10);
	}

	@Test
	public void testSaveProduct() {
		ProductDao c = ProductDao.getInstance();
		Tproduct pro = new Tproduct();
		pro.setVersion("Product Testing Save");
		c.saveTproduct(pro);
		
		Tproduct test = c.getTproduct(pro.getOid());
		assertNotNull(test);
	}

	@Test
	public void testDeleteProduct() {
		ProductDao c = ProductDao.getInstance();
		List<Tproduct> products = c.getTproducts();
		for (Tproduct pro : products) {
			c.deleteTproduct(pro.getOid());
		}
		products = c.getTproducts();
		assertTrue(products.size() < 1);
	}

}
