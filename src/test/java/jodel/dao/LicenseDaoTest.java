package jodel.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.PrepareTests;
import jodel.model.Tlicense;

public class LicenseDaoTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		PrepareTests.initDatabase();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetInstance() {
		LicenseDao c = LicenseDao.getInstance();
		assertNotNull(c);
	}

	@Test
	public void testGetLicense() {
		LicenseDao c = LicenseDao.getInstance();
		Tlicense lic = new Tlicense();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d1 = cal.getTime();

		lic.setKey(56789);
		lic.setExpire(d1);
		lic.setIp1("LicenseDaoTest");
		lic.setIp2("test");
		lic.setIp3("Get");
		lic.setIp4("License()");

		c.saveTlicense(lic);
		Tlicense test = c.getTlicense(lic.getOid());
		assertNotNull(test);
	}

	@Test
	public void testGetlicenses() {
		LicenseDao c = LicenseDao.getInstance();
		Tlicense lic;

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d1 = cal.getTime();

		for (long i = 0; i < 10; ++i) {
			lic = new Tlicense();

			lic.setKey((int) (123456789 + i));
			lic.setExpire(d1);
			lic.setIp1("LicenseDaoTest");
			lic.setIp2("test");
			lic.setIp3("Get");
			lic.setIp4("Licenses()");
			
			c.saveTlicense(lic);
		}
		List<Tlicense> licenses = c.getTlicenses();
		assertTrue(licenses.size() >= 10);
	}

	@Test
	public void testSaveLicense() {
		LicenseDao c = LicenseDao.getInstance();
		Tlicense lic = new Tlicense();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d1 = cal.getTime();

		lic.setKey(56789);
		lic.setExpire(d1);
		lic.setIp1("LicenseDaoTest");
		lic.setIp2("test");
		lic.setIp3("Save");
		lic.setIp4("Licenses()");

		c.saveTlicense(lic);
		Tlicense lics = c.getTlicense(lic.getOid());
		assertNotNull(lics);
	}

	@Test
	public void testDeleteLicense() {
		LicenseDao c = LicenseDao.getInstance();
		List<Tlicense> licenses = c.getTlicenses();
		for (Tlicense com : licenses) {
			c.deleteTlicense(com.getOid());
		}
		licenses = c.getTlicenses();
		assertTrue(licenses.size() < 1);
	}

}
