package jodel;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import jodel.dao.*;
import jodel.resources.*;

@RunWith(Suite.class)
@SuiteClasses({
			DaoManagerTest.class,
			CompanyDaoTest.class,
			LicenseDaoTest.class,
			ProductDaoTest.class,
			ServiceContractDaoTest.class,
			UserDaoTest.class,
			CompaniesResourceTest.class,
			CompanyResourceTest.class,
			LicenseResourceTest.class,
			ProductResourceTest.class,
			ServiceContractResourceTest.class,
			UserResourceTest.class
			   })
public class AllTests {

}
