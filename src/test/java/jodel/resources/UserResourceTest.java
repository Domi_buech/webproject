package jodel.resources;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.UserDao;


public class UserResourceTest {
	private static Tuser user;
	private static UserResource userResource;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
		user = new Tuser();
		
		user.setFirstname("Testing");
		user.setLastname("UserResourceTest");
		user.setActive((byte) 0);
		user.setAdmin((byte)1);
		user.setEmail("test.test@admin.de");
		user.setLoginname("hansi");
		user.setPassword("geheim");
		UserDao.getInstance().saveTuser(user);
		userResource = new UserResource(null, null, user.getOid());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void testGetTuser(){
		assertEquals(userResource.getTuser(),user);
	}
	
	@Test
	public void testGetTuserHtml(){
		assertNotEquals(userResource.getTuserHTML(),"Get: Tuser with " + user.getOid() +  " not found");
	}
	
	@Test
	public void testDeleteTuser(){
		userResource.deleteTuser();
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("Get: Tuser with " + user.getOid() + " not found");
		userResource.getTuserHTML();
	}
}
