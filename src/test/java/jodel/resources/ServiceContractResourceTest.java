package jodel.resources;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.ServiceContractDao;

public class ServiceContractResourceTest {
	private static Tservicecontract contract;
	private static ServiceContractResource contractResource;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		contract = new Tservicecontract();
		
		Calendar cal = Calendar.getInstance();
		Date d1 = cal.getTime();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d2 = cal.getTime();

		contract.setStart(d1);
		contract.setEnd(d2);
		ServiceContractDao.getInstance().saveTservicecontract(contract);
		contractResource = new ServiceContractResource(null, null, contract.getOid());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testGetServiceContract() {
		assertEquals(contractResource.getServiceContract(), contract);
	}

	@Test
	public void testGetServiceContractHtml() {
		try {
			contractResource.getServiceContractHTML();
		} catch (RuntimeException e) {
			assertTrue(false);
		}
		assertTrue(true);

	}

	@Test
	public void testDeleteServiceContract() {
		contractResource.deleteServiceContract();
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("Get: Service contract with " + contract.getOid() + " not found");
		contractResource.getServiceContractHTML();
	}
}
