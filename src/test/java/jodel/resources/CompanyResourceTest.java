package jodel.resources;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.CompanyDao;


public class CompanyResourceTest {
	private static Tcompany company;
	private static CompanyResource companyResource;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		company = new Tcompany();
		company.setName("CompanyResourceTest");
		company.setDepartment("IT");
		company.setStreetnr("24/B");
		company.setStreet("Valley Drive");
		company.setPostalcode(12345);
		company.setCountry("USA");
		CompanyDao.getInstance().saveTcompany(company);
		companyResource = new CompanyResource(null, null, company.getOid());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Rule 
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void testGetCompany(){
		assertEquals(companyResource.getCompany(),company);
	}
	
	@Test
	public void testGetCompanyHtml(){
		try{
			companyResource.getCompanyHTML();
		}
		catch(RuntimeException e){
			assertTrue(false);
		}
		assertTrue(true);
		
	}
	
	@Test
	public void testDeleteCompany(){
		companyResource.deleteCompany();
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("Get: Company with " + company.getOid() +  " not found");
		companyResource.getCompanyHTML();
	}
}
