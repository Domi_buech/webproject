package jodel.resources;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.ProductDao;


public class ProductResourceTest {
	private static Tproduct product;
	private static ProductResource productResource;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		product = new Tproduct();
		product.setVersion("Product Testing Get");
		ProductDao.getInstance().saveTproduct(product);
		productResource = new ProductResource(null, null, product.getOid());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Rule 
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void testGetProduct(){
		assertEquals(productResource.getProduct(),product);
	}
	
	@Test
	public void testGetProductHtml(){
		try{
			productResource.getProductHTML();
		}
		catch(RuntimeException e){
			assertTrue(false);
		}
		assertTrue(true);
		
	}
	
	@Test
	public void testDeleteProduct(){
		productResource.deleteProduct();
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("Get: Product with " + product.getOid() +  " not found");
		productResource.getProductHTML();
	}
}
