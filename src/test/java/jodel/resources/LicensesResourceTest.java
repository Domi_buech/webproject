package jodel.resources;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.LicenseDao;

public class LicensesResourceTest {
	private static LicenseDao c;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		c = LicenseDao.getInstance();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test 
	public void testGetProducts(){
		LicensesResource licensesResource = new LicensesResource();
		int count1 = licensesResource.getLicenses().size();
		Tlicense license = new Tlicense();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d1 = cal.getTime();
		
		license.setExpire(d1);
		license.setKey(12345);
		license.setIp1("Test-123");
		license.setIp2("Test-123");
		license.setIp3("Test-123");
		license.setIp4("Test-123");
		
		c.saveTlicense(license);
		int count2 = licensesResource.getLicenses().size();
		assertEquals(count2 - count1,1);
	}

	@Test
	public void testNewProduct() throws IOException {
		List<Tlicense> list = c.getTlicenses();
		int count1 = list.size();
		LicensesResource licensesResource = new LicensesResource();
		Tlicense license = new Tlicense();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d1 = cal.getTime();

		license.setExpire(d1);
		license.setKey(12345);
		license.setIp1("Test-123");
		license.setIp2("Test-123");
		license.setIp3("Test-123");
		license.setIp4("Test-123");
		c.saveTlicense(license);
		licensesResource.newTlicense(license, null);
		int count2 = c.getTlicenses().size();
		assertEquals(count2 - count1, 1);
	}

	@Test
	public void testGetCount() throws IOException {
		LicensesResource licensesResource = new LicensesResource();
		int count1 = Integer.parseInt(licensesResource.getCount());
		Tlicense license = new Tlicense();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d1 = cal.getTime();
		
		license.setExpire(d1);
		license.setKey(12345);
		license.setIp1("Test-123");
		license.setIp2("Test-123");
		license.setIp3("Test-123");
		license.setIp4("Test-123");
		c.saveTlicense(license);
		int count2 = Integer.parseInt(licensesResource.getCount());
		assertEquals(count2 - count1, 1);
	}

	@Test
	public void testGetProduct() {
		LicensesResource licensesResource = new LicensesResource();
		int count1 = Integer.parseInt(licensesResource.getCount());

		Tlicense license = new Tlicense();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d1 = cal.getTime();
		
		license.setExpire(d1);
		license.setKey(12345);
		license.setIp1("Test-123");
		license.setIp2("Test-123");
		license.setIp3("Test-123");
		license.setIp4("Test-123");
		c.saveTlicense(license);
		int count2 = Integer.parseInt(licensesResource.getCount());

		Tlicense license1 = new Tlicense();
		
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d2 = cal.getTime();
		
		license1.setExpire(d2);
		license1.setKey(12345);
		license1.setIp1("Test-123");
		license1.setIp2("Test-123");
		license1.setIp3("Test-123");
		license1.setIp4("Test-123");
		c.saveTlicense(license1);
		int count3 = Integer.parseInt(licensesResource.getCount());

		assertEquals(count2 - count1, 1);
		assertEquals(count3 - count2, 1);
	}

}
