package jodel.resources;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.ProductDao;

public class ProductsResourceTest {
	private static ProductDao c;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		c = ProductDao.getInstance();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test 
	public void testGetProducts(){
		ProductsResource ProductsResource = new ProductsResource();
		int count1 = ProductsResource.getProducts().size();
		Tproduct product = new Tproduct();
		
		product.setVersion("atest123");
		c.saveTproduct(product);
		int count2 = ProductsResource.getProducts().size();
		assertEquals(count2 - count1,1);
	}

	@Test
	public void testNewProduct() throws IOException {
		List<Tproduct> list = c.getTproducts();
		int count1 = list.size();
		ProductsResource productsResource = new ProductsResource();
		Tproduct product = new Tproduct();
		
		product.setVersion("atest123");
		c.saveTproduct(product);
		productsResource.newTproduct(product, null);
		int count2 = c.getTproducts().size();
		assertEquals(count2 - count1, 1);
	}

	@Test
	public void testGetCount() throws IOException {
		ProductsResource productsResource = new ProductsResource();
		int count1 = Integer.parseInt(productsResource.getCount());
		Tproduct product = new Tproduct();

		product.setVersion("atest123");
		c.saveTproduct(product);
		int count2 = Integer.parseInt(productsResource.getCount());
		assertEquals(count2 - count1, 1);
	}

	@Test
	public void testGetProduct() {
		ProductsResource productsResource = new ProductsResource();
		int count1 = Integer.parseInt(productsResource.getCount());

		Tproduct product1 = new Tproduct();

		product1.setVersion("atest123");
		c.saveTproduct(product1);
	
		int count2 = Integer.parseInt(productsResource.getCount());

		Tproduct product2 = new Tproduct();
		
		
		product2.setVersion("atest123");
		c.saveTproduct(product2);
		int count3 = Integer.parseInt(productsResource.getCount());

		assertEquals(count2 - count1, 1);
		assertEquals(count3 - count2, 1);
	}

}
