package jodel.resources;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.LicenseDao;


public class LicenseResourceTest {
	private static Tlicense license;
	private static LicenseResource licenseResource;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		license = new Tlicense();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d1 = cal.getTime();
		
		license.setKey(56789);
		license.setExpire(d1);
		license.setIp1("Testing");
		license.setIp2("License");
		license.setIp3("Resource");
		license.setIp4("Class");
		
		LicenseDao.getInstance().saveTlicense(license);
		licenseResource = new LicenseResource(null, null, license.getOid());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Rule 
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void testGetLicense(){
		assertEquals(licenseResource.getLicense(), license);
	}
	
	@Test
	public void testGetLicenseHtml(){
		try{
			licenseResource.getLicenseHTML();
		}
		catch(RuntimeException e){
			assertTrue(false);
		}
		assertTrue(true);
		
	}
	
	@Test
	public void testDeleteLicense(){
		licenseResource.deleteLicense();
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("Get: License with " + license.getOid() +  " not found");
		licenseResource.getLicenseHTML();
	}
}
