package jodel.resources;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.LicenseDao;
import jodel.dao.ServiceContractDao;

public class ServiceContractsResourceTest {
	private static ServiceContractDao c;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		c = ServiceContractDao.getInstance();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test 
	public void testGetServiceContracts(){
		ServiceContractsResource ServiceContractsResource = new ServiceContractsResource();
		int count1 = ServiceContractsResource.getServiceContracts().size();
		Tservicecontract sercon = new Tservicecontract();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 1); // increases date by 10 years
		Date d1 = cal.getTime();
		
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d2 = cal.getTime();
		
		sercon.setStart(d1);
		sercon.setEnd(d2);
		
		c.saveTservicecontract(sercon);
		int count2 = ServiceContractsResource.getServiceContracts().size();
		assertEquals(count2 - count1,1);
	}

	@Test
	public void testNewServiceContract() throws IOException {
		List<Tservicecontract> list = c.getTservicecontracts();
		int count1 = list.size();
		ServiceContractsResource ServiceContractsResource = new ServiceContractsResource();
		Tservicecontract sercon = new Tservicecontract();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 1); // increases date by 10 years
		Date d1 = cal.getTime();
		
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d2 = cal.getTime();
		
		sercon.setStart(d1);
		sercon.setEnd(d2);
		c.saveTservicecontract(sercon);
		ServiceContractsResource.newTservicecontract(sercon, null);
		int count2 = c.getTservicecontracts().size();
		assertEquals(count2 - count1, 1);
	}

	@Test
	public void testGetCount() throws IOException {
		ServiceContractsResource ServiceContractsResource = new ServiceContractsResource();
		int count1 = Integer.parseInt(ServiceContractsResource.getCount());
		Tservicecontract sercon = new Tservicecontract();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 1); // increases date by 10 years
		Date d1 = cal.getTime();
		
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d2 = cal.getTime();
		
		sercon.setStart(d1);
		sercon.setEnd(d2);
		c.saveTservicecontract(sercon);
		int count2 = Integer.parseInt(ServiceContractsResource.getCount());
		assertEquals(count2 - count1, 1);
	}

	@Test
	public void testGetServiceContract() {
		ServiceContractsResource ServiceContractsResource = new ServiceContractsResource();
		int count1 = Integer.parseInt(ServiceContractsResource.getCount());

		Tservicecontract sercon1 = new Tservicecontract();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 1); // increases date by 10 years
		Date d1 = cal.getTime();
		
		cal.add(Calendar.YEAR, 10); // increases date by 10 years
		Date d2 = cal.getTime();
		
		sercon1.setStart(d1);
		sercon1.setEnd(d2);
		c.saveTservicecontract(sercon1);
	
		int count2 = Integer.parseInt(ServiceContractsResource.getCount());

		Tservicecontract license2 = new Tservicecontract();
		
		license2.setStart(d1);
		license2.setEnd(d2);
		
		c.saveTservicecontract(license2);
		int count3 = Integer.parseInt(ServiceContractsResource.getCount());

		assertEquals(count2 - count1, 1);
		assertEquals(count3 - count2, 1);
	}

}
