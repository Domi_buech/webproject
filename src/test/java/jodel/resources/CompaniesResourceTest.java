package jodel.resources;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.CompanyDao;



public class CompaniesResourceTest {
	private static CompanyDao c;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		c = CompanyDao.getInstance();
	}
	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test 
	public void testGetCompanies(){
		CompaniesResource companiesResource = new CompaniesResource();
		int count1 = companiesResource.getCompanies().size();
		Tcompany company = new Tcompany();
		company.setName("ResourceTest");
		company.setDepartment("IT");
		company.setStreetnr("24/B");
		company.setStreet("Valley Drive");
		company.setPostalcode(12345);
		company.setCountry("USA");
		c.saveTcompany(company);
		int count2 = companiesResource.getCompanies().size();
		assertEquals(count2 - count1,1);
	}
	
	@Test
	public void testNewCompany() throws IOException {
		List<Tcompany> list = c.getTcompanies();
		int count1 = list.size();
		CompaniesResource companiesResource = new CompaniesResource();
		Tcompany company = new Tcompany();
		company.setName("ResourceTest2");
		company.setDepartment("IT");
		company.setStreetnr("24/B");
		company.setStreet("Valley Drive23");
		company.setPostalcode(5321);
		company.setCountry("USA");
		c.saveTcompany(company);
		companiesResource.newCompany(company, null);
		int count2 = c.getTcompanies().size(); 
		assertEquals(count2 - count1,1);
	}
	
	@Test
	public void testGetCount() throws IOException{
		CompaniesResource companiesResource = new CompaniesResource();
		int count1 = Integer.parseInt(companiesResource.getCount());
		Tcompany company = new Tcompany();
		company.setName("ResourceTest3");
		company.setDepartment("IT");
		company.setStreetnr("24/B");
		company.setStreet("Valley Drive3");
		company.setPostalcode(12345);
		company.setCountry("USA");
		c.saveTcompany(company);;
		int count2 = Integer.parseInt(companiesResource.getCount());
		assertEquals(count2 - count1,1);
	}
	@Test
	public void testGetCompany(){
		CompaniesResource companiesResource = new CompaniesResource();
		int count1 = Integer.parseInt(companiesResource.getCount());
		
		Tcompany company1 = new Tcompany();
		company1.setName("Testing CompaniesResource");
		company1.setDepartment("Marketing");
		company1.setStreetnr("101/C");
		company1.setStreet("Greek Drive");
		company1.setPostalcode(54321);
		company1.setCountry("Wakanda");
		CompanyDao.getInstance().saveTcompany(company1);
		int count2 = Integer.parseInt(companiesResource.getCount());
		
		Tcompany company2 = new Tcompany();
		company2.setName("Testing CompaniesResource");
		company2.setDepartment("Marketing");
		company2.setStreetnr("101/C");
		company2.setStreet("Greek Drive");
		company2.setPostalcode(54321);
		company2.setCountry("Uganda");
		CompanyDao.getInstance().saveTcompany(company2);
		int count3 = Integer.parseInt(companiesResource.getCount());

		assertEquals(count2 - count1, 1);
		assertEquals(count3 - count2, 1);
	}
	
}
