package jodel.resources;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jodel.model.*;
import jodel.dao.UserDao;

public class UsersResourceTest {
	private static UserDao c;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		c = UserDao.getInstance();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test 
	public void testGetUsers(){
		UsersResource UsersResource = new UsersResource();
		int count1 = UsersResource.getTusers().size();
		Tuser user = new Tuser();
		
		user.setFirstname("Testing");
		user.setLastname("UserResourceTest");
		user.setActive((byte) 0);
		user.setAdmin((byte)1);
		user.setEmail("test.test@admin.de");
		user.setLoginname("han235si");
		user.setPassword("geheim");
		UserDao.getInstance().saveTuser(user);
		
		int count2 = UsersResource.getTusers().size();
		assertEquals(count2 - count1,1);
	}

	@Test
	public void testNewUser() throws IOException {
		List<Tuser> list = c.getTusers();
		int count1 = list.size();
		UsersResource UsersResource = new UsersResource();
		Tuser user = new Tuser();
		
		user.setFirstname("Testing");
		user.setLastname("UserResourceTest");
		user.setActive((byte) 0);
		user.setAdmin((byte)1);
		user.setEmail("test.test@admin.de");
		user.setLoginname("han46434535si");
		user.setPassword("geheim");
		UserDao.getInstance().saveTuser(user);
		
		UsersResource.newTuser(user, null);
		int count2 = c.getTusers().size();
		assertEquals(count2 - count1, 1);
	}

	@Test
	public void testGetCount() throws IOException {
		UsersResource UsersResource = new UsersResource();
		int count1 = Integer.parseInt(UsersResource.getCount());
		Tuser user = new Tuser();
		
		user.setFirstname("Testing");
		user.setLastname("UserResourceTest");
		user.setActive((byte) 0);
		user.setAdmin((byte)1);
		user.setEmail("test.test@admin.de");
		user.setLoginname("han123si");
		user.setPassword("geheim");
		UserDao.getInstance().saveTuser(user);
		
		int count2 = Integer.parseInt(UsersResource.getCount());
		assertEquals(count2 - count1, 1);
	}

	@Test
	public void testGetUser() {
		UsersResource UsersResource = new UsersResource();
		int count1 = Integer.parseInt(UsersResource.getCount());

		Tuser user1 = new Tuser();
		
		user1.setFirstname("Testing");
		user1.setLastname("UserResourceTest");
		user1.setActive((byte) 0);
		user1.setAdmin((byte)1);
		user1.setEmail("test.test@admin.de");
		user1.setLoginname("han54s12");
		user1.setPassword("geheim");
		UserDao.getInstance().saveTuser(user1);
	
		int count2 = Integer.parseInt(UsersResource.getCount());

		Tuser user2 = new Tuser();
		
		user2.setFirstname("Testing");
		user2.setLastname("UserResourceTest");
		user2.setActive((byte) 0);
		user2.setAdmin((byte)1);
		user2.setEmail("test.test@admin.de");
		user2.setLoginname("han123si");
		user2.setPassword("geheim");
		UserDao.getInstance().saveTuser(user2);
		
		c.saveTuser(user2);
		int count3 = Integer.parseInt(UsersResource.getCount());

		assertEquals(count2 - count1, 1);
		assertEquals(count3 - count2, 1);
	}

}
