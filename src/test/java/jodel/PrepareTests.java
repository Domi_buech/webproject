package jodel;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.junit.Test;

import jodel.dao.DaoManager;

public class PrepareTests {

	@Test
	public void clearDB() {
		initDatabase();
	}

	public static void initDatabase() {
		DaoManager dm = DaoManager.getInstance();
		EntityManager em = dm.getEntityManager();

		EntityTransaction et = em.getTransaction();
		et = em.getTransaction();
		et.begin();
		try {
			em.createNativeQuery("DELETE FROM TCOMPANY").executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			em.createNativeQuery("DELETE FROM TSERVICECONTRACT").executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			em.createNativeQuery("DELETE FROM TPRODUCT").executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			em.createNativeQuery("DELETE FROM TLICENSE").executeUpdate();
		} catch (Exception e) {

		}

		try {
			em.createNativeQuery("DELETE FROM TUSER").executeUpdate();
		} catch (Exception e) {

		}
		et.commit();
		
		et = em.getTransaction();
		et.begin();
		et.commit();
	}

}
