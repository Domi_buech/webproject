package jodel.resources;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import jodel.dao.LicenseDao;
import jodel.dao.ProductDao;
import jodel.model.Tlicense;
import jodel.model.Tproduct;

// Will map the resource to the URL products
@Path("/products")
public class ProductsResource {

	// Allows to insert contextual objects into the class,
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	// Return the list of products to the user in the browser
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Tproduct> getProductsBrowser() {
		return ProductDao.getInstance().getTproducts();
	}

	// Return the list of products for applications
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Tproduct> getProducts() {
		return ProductDao.getInstance().getTproducts();
	}

	// returns the number of products
	// Use http://localhost:8080/Rest/rest/products/count
	// to get the total number of records
	@GET
	@Path("count")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCount() {
		int count = ProductDao.getInstance().getTproducts().size();
		return String.valueOf(count);
	}

	@POST
	@Path("/{productOid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Tproduct newTproduct(Tproduct product, @Context HttpServletResponse servletResponse) throws IOException {
		ProductDao c = ProductDao.getInstance();
		c.saveTproduct(product);
		Tproduct savedProduct = c.getTproduct(product.getOid());

		return savedProduct;
	}

	@DELETE
	@Path("/{productOid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Tproduct> deleteTproduct(@PathParam("productOid") Integer productOid,
			@Context HttpServletResponse servletResponse) throws IOException {
		ProductDao.getInstance().deleteTproduct(productOid);
		return ProductDao.getInstance().getTproducts();
	}

	// Defines that the next path parameter after products is
	// treated as a parameter and passed to the ProductResources
	// Allows to type http://localhost:8080/Rest/rest/products/1
	// 1 will be treaded as parameter and passed to ProductResource
	@GET
	@Path("{productId}")
	@Produces(MediaType.APPLICATION_JSON)
	public ProductResource getTproduct(@PathParam("productId") Integer id) {
		return new ProductResource(uriInfo, request, id);
	}

}
