package jodel.resources;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import jodel.dao.CompanyDao;
import jodel.dao.UserDao;
import jodel.model.Tcompany;
import jodel.model.Tservicecontract;
import jodel.model.Tuser;

// Will map the resource to the URL companies
@Path("/companies")
public class CompaniesResource {

	// Allows to insert contextual objects into the class,
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	// Return the list of companies to the user in the browser
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Tcompany> getCompaniesBrowser() {
		return CompanyDao.getInstance().getTcompanies();
	}

	// Return the list of companies for applications
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Tcompany> getCompanies() {
		return CompanyDao.getInstance().getTcompanies();
	}

	// returns the number of companies
	// Use http://localhost:8080/Rest/rest/companies/count
	// to get the total number of records
	@GET
	@Path("count")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCount() {
		int count = CompanyDao.getInstance().getTcompanies().size();
		return String.valueOf(count);
	}

	@POST
	@Path("/{companyOid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Tcompany> newCompany(Tcompany company, @Context HttpServletResponse servletResponse)
			throws IOException {
		CompanyDao.getInstance().saveTcompany(company);
		return CompanyDao.getInstance().getTcompanies();
	}

	@DELETE
	@Path("/{companyOid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Tcompany> deleteCompany(@PathParam("companyOid") Integer companyOid,
			@Context HttpServletResponse servletResponse) throws IOException {
		CompanyDao.getInstance().deleteTcompany(companyOid);
		return CompanyDao.getInstance().getTcompanies();
	}

	// Defines that the next path parameter after companies is
	// treated as a parameter and passed to the CompanyResources
	// Allows to type http://localhost:8080/Rest/rest/companies/1
	// 1 will be treaded as parameter and passed to CompanyResource
	@GET
	@Path("{companyOid}")
	@Produces(MediaType.APPLICATION_JSON)
	public CompanyResource getCompany(@PathParam("companyOid") Integer companyOid) {
		return new CompanyResource(uriInfo, request, companyOid);
	}

	@GET
	@Path("{companyOid}/employees")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Tuser> getCompanyEmployees(@PathParam("companyOid") Integer companyOid) {
		List<Tuser> employees = CompanyDao.getInstance().getTcompanyEmployees(companyOid);
		return employees;
	}

	@GET
	@Path("{companyOid}/sercons")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Tservicecontract> getCompanyServiceContracts(@PathParam("companyOid") Integer companyOid) {
		List<Tservicecontract> sercons = CompanyDao.getInstance().getTcompanyServiceContracts(companyOid);
		return sercons;
	}
	
}
