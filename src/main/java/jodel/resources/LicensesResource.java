package jodel.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import jodel.dao.LicenseDao;
import jodel.dao.UserDao;
import jodel.model.Tlicense;
import jodel.model.Tuser;

// Will map the resource to the URL licenses
@Path("/licenses")
public class LicensesResource {

	// Allows to insert contextual objects into the class,
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	// Return the list of licenses to the user in the browser
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Tlicense> getLicensesBrowser() {
		return LicenseDao.getInstance().getTlicenses();
	}

	// Return the list of licenses for applications
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Tlicense> getLicenses() {
		return LicenseDao.getInstance().getTlicenses();
	}

	// returns the number of licenses
	// Use http://localhost:8080/Rest/rest/licenses/count
	// to get the total number of records
	@GET
	@Path("count")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCount() {
		int count = LicenseDao.getInstance().getTlicenses().size();
		return String.valueOf(count);
	}

	@POST
	@Path("/{licenseOid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Tlicense> newTlicense(Tlicense license, @Context HttpServletResponse servletResponse)
			throws IOException {
		LicenseDao.getInstance().saveTlicense(license);
		return LicenseDao.getInstance().getTlicenses();
	}

	@DELETE
	@Path("/{licenseOid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Tlicense> deleteTlicense(@PathParam("licenseOid") Integer licenseOid,
			@Context HttpServletResponse servletResponse) throws IOException {
		LicenseDao.getInstance().deleteTlicense(licenseOid);
		return LicenseDao.getInstance().getTlicenses();
	}

	// Defines that the next path parameter after licenses is
	// treated as a parameter and passed to the LicenseResources
	// Allows to type http://localhost:8080/Rest/rest/licenses/1
	// 1 will be treaded as parameter and passed to LicenseResource
	@GET
	@Path("/{licenseOid}")
	@Produces(MediaType.APPLICATION_JSON)
	public LicenseResource getTlicense(@PathParam("licenseOid") Integer licenseOid) throws IOException {

		return new LicenseResource(uriInfo, request, licenseOid);
	}

}
