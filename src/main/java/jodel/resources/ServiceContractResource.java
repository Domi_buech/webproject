package jodel.resources;


import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

import jodel.dao.ServiceContractDao;
import jodel.model.Tservicecontract;

public class ServiceContractResource {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	Integer id;

	public ServiceContractResource(UriInfo uriInfo, Request request, Integer id) {
		this.uriInfo = uriInfo;
		this.request = request;
		this.id = id;
	}

	//Application integration     
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Tservicecontract getServiceContract() {
		Tservicecontract servicecontract = ServiceContractDao.getInstance().getTservicecontract(id);
		if(servicecontract==null)
			throw new RuntimeException("Get: Servicec contract with " + id +  " not found");
		return servicecontract;
	}

	// for the browser
	@GET
	@Produces(MediaType.TEXT_XML)
	public Tservicecontract getServiceContractHTML() {
		Tservicecontract servicecontract = ServiceContractDao.getInstance().getTservicecontract(id);
		if(servicecontract==null)
			throw new RuntimeException("Get: Service contract with " + id +  " not found");
		return servicecontract;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response putServiceContract(JAXBElement<Tservicecontract> servicecontract) {
		Tservicecontract c = servicecontract.getValue();
		return putAndGetResponse(c);
	}

	@DELETE
	public void deleteServiceContract() {
		ServiceContractDao.getInstance().deleteTservicecontract(id);
	}

	private Response putAndGetResponse(Tservicecontract servicecontract) {
		Response res;
		ServiceContractDao.getInstance().saveTservicecontract(servicecontract);
		res = Response.created(uriInfo.getAbsolutePath()).build();
		return res;
	}
} 