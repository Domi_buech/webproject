package jodel.resources;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import jodel.dao.UserDao;
import jodel.model.Tuser;

// Will map the resource to the URL users
@Path("/users")
public class UsersResource {

	// Allows to insert contextual objects into the class,
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	// Return the list of users to the user in the browser
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Tuser> getTusersBrowser() {
		return UserDao.getInstance().getTusers();
	}

	// Return the list of users in JSON format
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Tuser> getTusers() {
		System.out.println("getTusers()");
		return UserDao.getInstance().getTusers();
	}

	// returns the number of users
	// Use http://localhost:8080/Jodel/rest/users/count
	// to get the total number of records
	@GET
	@Path("count")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCount() {
		int count = UserDao.getInstance().getTusers().size();
		return String.valueOf(count);
	}

	@POST
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> appLogin(Tuser user, @Context HttpServletResponse servletResponse) {
		Map<String, Object> response = new HashMap<>();
		System.out.println("Reached appLogin function");
		try {
			Tuser checkUser = UserDao.getInstance().getTuserByLoginName(user.getLoginname());
			if (checkUser != null) {
				System.out.println("Reached appLogin function: inside if");
				String password1 = checkUser.getPassword();
				String password2 = user.getPassword();

				if (password1.equals(password2)) {
					response.put("success", true);
					Integer oid = checkUser.getOid();
					System.out.println("Reached appLogin function: second if");
					response.put("oid", oid);
					response.put("tcompany_OID", checkUser.getTcompany());
					response.put("admin", checkUser.getAdmin());
				} else {
					System.out.println("if not called");
					response.put("success", false);
				}
			}
		} catch (NoResultException ioe) {
			response.put("notFound", true);
		}

		return response;
	}

	// This is the workhorse
	@POST
	@Path("/{userPK}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Tuser newTuser(Tuser user, @Context HttpServletResponse servletResponse) throws IOException {
		UserDao.getInstance().saveTuser(user);
		return UserDao.getInstance().getTuserByLoginName(user.getLoginname());
	}

	@DELETE
	@Path("/{userOid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Tuser> deleteTuser(@PathParam("userOid") Integer userOid, @Context HttpServletResponse servletResponse)
			throws IOException {
		UserDao.getInstance().deleteTuser(userOid);
		return UserDao.getInstance().getTusers();
	}

	// Defines that the next path parameter after users is
	// treated as a parameter and passed to the UserResources
	// Allows to type http://localhost:8080/Rest/rest/users/1
	// 1 will be treaded as parameter and passed to UserResource
	@GET
	@Path("/{userOid}")
	@Produces(MediaType.APPLICATION_JSON)
	public UserResource getTuser(@PathParam("userOid") Integer userOid) throws IOException {
		return new UserResource(uriInfo, request, userOid);
	}

}
