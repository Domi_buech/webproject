package jodel.resources;


import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

import jodel.dao.ProductDao;
import jodel.model.Tproduct;

public class ProductResource {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	Integer id;

	public ProductResource(UriInfo uriInfo, Request request, Integer id) {
		this.uriInfo = uriInfo;
		this.request = request;
		this.id = id;
	}

	//Application integration     
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Tproduct getProduct() {
		Tproduct product = ProductDao.getInstance().getTproduct(id);
		if(product==null)
			throw new RuntimeException("Get: Product with " + id +  " not found");
		return product;
	}

	// for the browser
	@GET
	@Produces(MediaType.TEXT_XML)
	public Tproduct getProductHTML() {
		Tproduct product = ProductDao.getInstance().getTproduct(id);
		if(product==null)
			throw new RuntimeException("Get: Product with " + id +  " not found");
		return product;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response putProduct(JAXBElement<Tproduct> product) {
		Tproduct c = product.getValue();
		return putAndGetResponse(c);
	}

	@DELETE
	public void deleteProduct() {
		ProductDao.getInstance().deleteTproduct(id);
	}

	private Response putAndGetResponse(Tproduct product) {
		Response res;
		ProductDao.getInstance().saveTproduct(product);
		res = Response.created(uriInfo.getAbsolutePath()).build();
		return res;
	}
} 