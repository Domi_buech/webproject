package jodel.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import jodel.dao.ProductDao;
import jodel.dao.ServiceContractDao;
import jodel.model.Tproduct;
import jodel.model.Tservicecontract;

// Will map the resource to the URL licenses
@Path("/sercons")
public class ServiceContractsResource {

	// Allows to insert contextual objects into the class,
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	// Return the list of ServiceContracts to the user in the browser
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Tservicecontract> getServiceContractsBrowser() {
		return ServiceContractDao.getInstance().getTservicecontracts();
	}

	// Return the list of licenses for applications
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Tservicecontract> getServiceContracts() {
		return ServiceContractDao.getInstance().getTservicecontracts();
	}

	// returns the number of licenses
	// Use http://localhost:8080/Rest/rest/licenses/count
	// to get the total number of records
	@GET
	@Path("count")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCount() {
		int count = ServiceContractDao.getInstance().getTservicecontracts().size();
		return String.valueOf(count);
	}

	@POST
	@Path("/{serconOid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Tservicecontract newTservicecontract(Tservicecontract sercon, @Context HttpServletResponse servletResponse)
			throws IOException {
		ServiceContractDao c = ServiceContractDao.getInstance();
		c.saveTservicecontract(sercon);
		Tservicecontract savedContract = c.getTservicecontract(sercon.getOid());
		return savedContract;
	}

	@DELETE
	@Path("/{serconOid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Tservicecontract> deleteTservicecontract(@PathParam("serconOid") Integer serconOid,
			@Context HttpServletResponse servletResponse) throws IOException {
		ServiceContractDao.getInstance().deleteTservicecontract(serconOid);
		return ServiceContractDao.getInstance().getTservicecontracts();
	}

	// Defines that the next path parameter after licenses is
	// treated as a parameter and passed to the LicenseResources
	// Allows to type http://localhost:8080/Rest/rest/sercons/1/1
	// 1s will be treaded as parameter and passed to ServiceContractResource
	@GET
	@Path("/{serconOid}")
	@Produces(MediaType.APPLICATION_JSON)
	public ServiceContractResource getTservicecontract(@PathParam("serconOid") Integer serconOid) throws IOException {
		return new ServiceContractResource(uriInfo, request, serconOid);
	}

}
