package jodel.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tlicense database table.
 * 
 */
@Entity
@NamedQuery(name="Tlicense.findAll", query="SELECT t FROM Tlicense t")
public class Tlicense implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TLICENSE_OID_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TLICENSE_OID_GENERATOR")
	private int oid;

	@Temporal(TemporalType.TIMESTAMP)
	private Date expire;

	private String ip1;

	private String ip2;

	private String ip3;

	private String ip4;

	@Column(name="`KEY`")
	private int key;

	@Column(name="LICENSE_COUNT")
	private int licenseCount;

	private int tservicecontract_OID;

	private int tservicecontract_TCOMPANY_OID;

	//bi-directional many-to-one association to Tproduct
	@ManyToOne
	@JoinColumn(name="tproduct_OID")
	private Tproduct tproduct;

	public Tlicense() {
	}

	public int getOid() {
		return this.oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public Date getExpire() {
		return this.expire;
	}

	public void setExpire(Date expire) {
		this.expire = expire;
	}

	public String getIp1() {
		return this.ip1;
	}

	public void setIp1(String ip1) {
		this.ip1 = ip1;
	}

	public String getIp2() {
		return this.ip2;
	}

	public void setIp2(String ip2) {
		this.ip2 = ip2;
	}

	public String getIp3() {
		return this.ip3;
	}

	public void setIp3(String ip3) {
		this.ip3 = ip3;
	}

	public String getIp4() {
		return this.ip4;
	}

	public void setIp4(String ip4) {
		this.ip4 = ip4;
	}

	public int getKey() {
		return this.key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public int getLicenseCount() {
		return this.licenseCount;
	}

	public void setLicenseCount(int licenseCount) {
		this.licenseCount = licenseCount;
	}

	public int getTservicecontract_OID() {
		return this.tservicecontract_OID;
	}

	public void setTservicecontract_OID(int tservicecontract_OID) {
		this.tservicecontract_OID = tservicecontract_OID;
	}

	public int getTservicecontract_TCOMPANY_OID() {
		return this.tservicecontract_TCOMPANY_OID;
	}

	public void setTservicecontract_TCOMPANY_OID(int tservicecontract_TCOMPANY_OID) {
		this.tservicecontract_TCOMPANY_OID = tservicecontract_TCOMPANY_OID;
	}

	public Tproduct getTproduct() {
		return this.tproduct;
	}

	public void setTproduct(Tproduct tproduct) {
		this.tproduct = tproduct;
	}

}