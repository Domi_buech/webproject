package jodel.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tservicecontract database table.
 * 
 */
@Entity
@NamedQuery(name="Tservicecontract.findAll", query="SELECT t FROM Tservicecontract t")
public class Tservicecontract implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TSERVICECONTRACT_OID_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TSERVICECONTRACT_OID_GENERATOR")
	private int oid;

	@Temporal(TemporalType.TIMESTAMP)
	private Date end;

	@Temporal(TemporalType.TIMESTAMP)
	private Date start;

	//bi-directional many-to-one association to Tcompany
	@ManyToOne
	@JoinColumn(name="tcompany_OID")
	private Tcompany tcompany;

	public Tservicecontract() {
	}

	public int getOid() {
		return this.oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public Date getEnd() {
		return this.end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Date getStart() {
		return this.start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Tcompany getTcompany() {
		return this.tcompany;
	}

	public void setTcompany(Tcompany tcompany) {
		this.tcompany = tcompany;
	}

}