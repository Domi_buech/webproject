package jodel.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tcompany database table.
 * 
 */
@Entity
@NamedQuery(name="Tcompany.findAll", query="SELECT t FROM Tcompany t")
public class Tcompany implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TCOMPANY_OID_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCOMPANY_OID_GENERATOR")
	private int oid;

	private String country;

	private String department;

	private String name;

	private int postalcode;

	private String street;

	private String streetnr;

	//bi-directional many-to-one association to Tservicecontract
	@OneToMany(mappedBy="tcompany")
	private List<Tservicecontract> tservicecontracts;

	//bi-directional many-to-one association to Tuser
	@OneToMany(mappedBy="tcompany")
	private List<Tuser> tusers;

	public Tcompany() {
	}

	public int getOid() {
		return this.oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPostalcode() {
		return this.postalcode;
	}

	public void setPostalcode(int postalcode) {
		this.postalcode = postalcode;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreetnr() {
		return this.streetnr;
	}

	public void setStreetnr(String streetnr) {
		this.streetnr = streetnr;
	}

	public List<Tservicecontract> getTservicecontracts() {
		return this.tservicecontracts;
	}

	public void setTservicecontracts(List<Tservicecontract> tservicecontracts) {
		this.tservicecontracts = tservicecontracts;
	}

	public Tservicecontract addTservicecontract(Tservicecontract tservicecontract) {
		getTservicecontracts().add(tservicecontract);
		tservicecontract.setTcompany(this);

		return tservicecontract;
	}

	public Tservicecontract removeTservicecontract(Tservicecontract tservicecontract) {
		getTservicecontracts().remove(tservicecontract);
		tservicecontract.setTcompany(null);

		return tservicecontract;
	}

	public List<Tuser> getTusers() {
		return this.tusers;
	}

	public void setTusers(List<Tuser> tusers) {
		this.tusers = tusers;
	}

	public Tuser addTuser(Tuser tuser) {
		getTusers().add(tuser);
		tuser.setTcompany(this);

		return tuser;
	}

	public Tuser removeTuser(Tuser tuser) {
		getTusers().remove(tuser);
		tuser.setTcompany(null);

		return tuser;
	}

}