package jodel.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tproduct database table.
 * 
 */
@Entity
@NamedQuery(name="Tproduct.findAll", query="SELECT t FROM Tproduct t")
public class Tproduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TPRODUCT_OID_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TPRODUCT_OID_GENERATOR")
	private int oid;

	private String version;

	//bi-directional many-to-one association to Tlicense
	@OneToMany(mappedBy="tproduct")
	private List<Tlicense> tlicenses;

	public Tproduct() {
	}

	public int getOid() {
		return this.oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<Tlicense> getTlicenses() {
		return this.tlicenses;
	}

	public void setTlicenses(List<Tlicense> tlicenses) {
		this.tlicenses = tlicenses;
	}

	public Tlicense addTlicens(Tlicense tlicens) {
		getTlicenses().add(tlicens);
		tlicens.setTproduct(this);

		return tlicens;
	}

	public Tlicense removeTlicens(Tlicense tlicens) {
		getTlicenses().remove(tlicens);
		tlicens.setTproduct(null);

		return tlicens;
	}

}