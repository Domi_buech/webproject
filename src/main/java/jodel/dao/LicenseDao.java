package jodel.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import jodel.model.Tcompany;
import jodel.model.Tlicense;
import jodel.model.Tuser;


public class LicenseDao {
	private static EntityManager em;
	private static LicenseDao singleton;

	private LicenseDao() {
		em = DaoManager.getInstance().getEntityManager();
	}

	public static LicenseDao getInstance() {
		if (singleton == null) {
			singleton = new LicenseDao();
		}
		return singleton;
	}

	public Tlicense getTlicense(Integer id) {
		return em.find(Tlicense.class, id);
	}

	public List<Tlicense> getTlicenses() {
		Query q = em.createQuery("select c from Tlicense c");
		List<Tlicense> licenses = q.getResultList();
		return licenses;
	}

	public void saveTlicense(Tlicense license) {
		int testedId = license.getOid();
		
		if(em.find(Tlicense.class, testedId) != null) {
			em.getTransaction().begin();
			em.merge(license);
			em.getTransaction().commit();
		} else {
		em.getTransaction().begin();
		em.persist(license);
		em.getTransaction().commit();
		}
	}

	public void deleteTlicense(Integer id) {
		Tlicense license = em.find(Tlicense.class, id);
		if (license != null) {
			em.getTransaction().begin();
			em.remove(license);
			em.getTransaction().commit();
		}
	}
}
