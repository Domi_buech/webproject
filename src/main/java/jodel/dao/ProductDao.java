package jodel.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import jodel.model.Tcompany;
import jodel.model.Tproduct;

public class ProductDao {
	private static EntityManager em;
	private static ProductDao singleton;

	private ProductDao() {
		em = DaoManager.getInstance().getEntityManager();
	}

	public static ProductDao getInstance() {
		if (singleton == null) {
			singleton = new ProductDao();
		}
		return singleton;
	}

	public Tproduct getTproduct(Integer id) {
		return em.find(Tproduct.class, id);
	}

	public List<Tproduct> getTproducts() {
		Query q = em.createQuery("select c from Tproduct c");
		List<Tproduct> products = q.getResultList();
		return products;
	}

	public void saveTproduct(Tproduct product) {
		Integer testedId = product.getOid();

		if (em.find(Tproduct.class, testedId) != null) {
			em.getTransaction().begin();
			em.merge(product);
			em.getTransaction().commit();
		} else {
			em.getTransaction().begin();
			em.persist(product);
			em.getTransaction().commit();
		}

	}

	public void deleteTproduct(Integer id) {
		Tproduct product = em.find(Tproduct.class, id);
		if (product != null) {
			em.getTransaction().begin();
			em.remove(product);
			em.getTransaction().commit();
		}
	}
}
