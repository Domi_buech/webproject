package jodel.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import jodel.model.Tuser;

public class UserDao {
	private static EntityManager em;
	private static UserDao singleton;

	private UserDao() {
		em = DaoManager.getInstance().getEntityManager();
	}

	public static UserDao getInstance() {
		if (singleton == null) {
			singleton = new UserDao();
		}
		return singleton;
	}

	public Tuser getTuser(Integer id) {
		return em.find(Tuser.class, id);
	}

	public Tuser getTuserByLoginName(String loginName) {
		Query q = em.createNativeQuery("SELECT * FROM Tuser c WHERE LOGINNAME='" + loginName + "'", Tuser.class);
		Tuser user = (Tuser) q.getSingleResult();
		return user;
	}

	public List<Tuser> getTusers() {
		Query q = em.createQuery("SELECT c FROM Tuser c");
		List<Tuser> users = q.getResultList();
		return users;
	}

	public void saveTuser(Tuser user) {
		Integer testedId = user.getOid();

		if (em.find(Tuser.class, testedId) != null) {
			em.getTransaction().begin();
			em.merge(user);
			em.getTransaction().commit();
		} else {
			em.getTransaction().begin();
			em.persist(user);
			em.getTransaction().commit();
		}
	}

	public void deleteTuser(Integer id) {
		Tuser user = em.find(Tuser.class, id);
		if (user != null) {
			em.getTransaction().begin();
			em.remove(user);
			em.getTransaction().commit();
		}
	}
}
