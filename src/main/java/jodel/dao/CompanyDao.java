package jodel.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import jodel.model.Tcompany;
import jodel.model.Tuser;
import jodel.model.Tservicecontract;

public class CompanyDao {
	private static EntityManager em;
	private static CompanyDao singleton;

	
	private CompanyDao() {
		em = DaoManager.getInstance().getEntityManager();
	}

	
	public static CompanyDao getInstance() {
		if (singleton == null) {
			singleton = new CompanyDao();
		}
		return singleton;
	}

	
	public Tcompany getTcompany(Integer id) {
		Tcompany test = em.find(Tcompany.class, id);
		return em.find(Tcompany.class, id);
	}
	
	
	public List<Tuser> getTcompanyEmployees(Integer id) {
		Tcompany test = em.find(Tcompany.class, id);
		List<Tuser> employees = test.getTusers();
		return employees;
	}
	
	public List<Tservicecontract> getTcompanyServiceContracts(Integer id) {
		Tcompany test = em.find(Tcompany.class, id);
		List<Tservicecontract> sercons = test.getTservicecontracts();
		return sercons;
	}
	
	public List<Tcompany> getTcompanies() {
		Query q = em.createQuery("select t from Tcompany t");
		List<Tcompany> companies = q.getResultList();
		return companies;
	}
	
	public void saveTcompany(Tcompany company) {
		Integer testedId = company.getOid();
		
		if(em.find(Tcompany.class, testedId) != null) {
			em.getTransaction().begin();
			em.merge(company);
			em.getTransaction().commit();
		} else {	
			em.getTransaction().begin();
			em.persist(company);
			em.getTransaction().commit();			
		}
		
	}

	
	public void deleteTcompany(Integer id) {
		Tcompany company = em.find(Tcompany.class, id);
		if (company != null) {
			em.getTransaction().begin();
			em.remove(company);
			em.getTransaction().commit();
		}
	}
}
