package jodel.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import jodel.model.Tcompany;
import jodel.model.Tservicecontract;
import jodel.model.Tuser;

public class ServiceContractDao {
	private static EntityManager em;
	private static ServiceContractDao singleton;

	private ServiceContractDao() {
		em = DaoManager.getInstance().getEntityManager();
	}

	public static ServiceContractDao getInstance() {
		if (singleton == null) {
			singleton = new ServiceContractDao();
		}
		return singleton;
	}

	public Tservicecontract getTservicecontract(Integer id) {
		return em.find(Tservicecontract.class, id);
	}

	public List<Tservicecontract> getTservicecontracts() {
		Query q = em.createQuery("select c from Tservicecontract c");
		List<Tservicecontract> sercons = q.getResultList();
		return sercons;
	}

	public void saveTservicecontract(Tservicecontract contract) {
		Integer testedId = contract.getOid();

		if (em.find(Tservicecontract.class, testedId) != null) {
			em.getTransaction().begin();
			em.merge(contract);
			em.getTransaction().commit();
		} else {
			em.getTransaction().begin();
			em.persist(contract);
			em.getTransaction().commit();
		}
	}

	public void deleteTservicecontract(Integer id) {
		Tservicecontract contract = em.find(Tservicecontract.class, id);
		if (contract != null) {
			em.getTransaction().begin();
			em.remove(contract);
			em.getTransaction().commit();
		}
	}
}
